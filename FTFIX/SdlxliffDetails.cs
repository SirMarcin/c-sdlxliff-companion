﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTFIX
{
    public class SdlxliffDetails
    {
        public string filePath {get; private set;}
        public string originalFilePath {get; private set;}
        public string fileTypeID{get; private set;}
        public string srcLang{get; private set;}
        public string tgtLang{get; private set;}
        public string fileName{get; private set;}
        public string untaskedFilePath { get; private set; }
        public string langChange { get; private set; }

        public SdlxliffDetails(string filePath, string originalFilePath, string fileTypeID, string srcLang, string tgtLang, string langChange)
        {
            this.filePath = filePath;
            this.originalFilePath = originalFilePath;
            this.fileTypeID = fileTypeID;
            this.srcLang = srcLang;
            this.tgtLang = tgtLang;
            this.langChange = langChange;
            fileName = untaskedFilePath = Path.GetFileName(filePath);
        }

        public void SetFilePath(string newFilePath){
            this.filePath = newFilePath;
        }
        public void SetUntaskedFilePath(string newUntaskedFilePath)
        {
            this.untaskedFilePath = newUntaskedFilePath;
        }
    }
}
