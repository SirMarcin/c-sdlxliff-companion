﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FTFIX
{
    public class Sdlxliff
    {
        private string originalFilePath;
        private string filePath;
        private string fileName;
        private string srcLang;
        private string tgtLang;
        private string fileTypeID;
        public string externalFile { get; private set; }
        private XmlDocument sdlxliff;
        private Sdlxliff srcSdlxliff;
        public bool isSource
        {
            get;
            private set;
        }
        
        public Sdlxliff(string filePath)
        {
            sdlxliff = new XmlDocument();
            sdlxliff.PreserveWhitespace = true;

            try
            {
                sdlxliff.Load(filePath);
            }
            catch (Exception sdlxliffLoadError)
            {
                Console.WriteLine("Problem loading SDLXLIFF file in constructor: " + sdlxliffLoadError.ToString());
                this.sdlxliff = null;
                throw(new Exception("Could not load .sdlxliff file.\r\n" + sdlxliffLoadError.ToString()));
            }

            if (sdlxliff != null)
            {
                this.filePath = filePath;
                this.fileTypeID = ReadFileTypeID();
                this.srcLang = ReadSrcLang();
                this.tgtLang = ReadTgtLang();
                this.externalFile = ReadExternalFile();

                if (tgtLang != "")
                    this.isSource = false;
                else
                {
                    this.isSource = true;
                }

                FileInfo fileInfo = new FileInfo(filePath);

                this.fileName = fileInfo.Name;
                originalFilePath = sdlxliff.SelectSingleNode("//*[local-name() = 'file']").Attributes["original"].InnerText;
            }
            else
            {
                Console.WriteLine("SDLXLIFF file not set, won't initialize it");
            }

        }

        private string ReadFileTypeID()
        {
            string fileTypeID = "";

            if (sdlxliff != null)
            {
                fileTypeID = this.sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']").InnerText;
            }

            return fileTypeID;
        }
        private string ReadSrcLang()
        {
            string srcLang = "";

            if (sdlxliff != null)
            {
                try
                {
                    srcLang = this.sdlxliff.SelectSingleNode("//*[local-name() = 'file']").Attributes["source-language"].InnerText;
                }
                catch (Exception srcExc)
                {
                    Console.WriteLine("No src lang set");
                    srcLang = "";
                }
            }

            return srcLang;
        }
        private string ReadTgtLang()
        {
            string tgtLang = "";

            if (sdlxliff != null)
            {
                try
                {
                    tgtLang = this.sdlxliff.SelectSingleNode("//*[local-name() = 'file']").Attributes["target-language"].InnerText;
                }
                catch (Exception tgtExc)
                {
                    tgtLang = "";
                }
            }

            return tgtLang;
        }
        private string ReadExternalFile()
        {
            string extFile = "";
            if (sdlxliff != null)
            {
                try
                {
                    extFile = this.sdlxliff.SelectSingleNode("//*[local-name() = 'external-file']").Attributes["href"].InnerText;
                }
                catch (Exception tgtExc)
                {
                    extFile = "";
                }
            }

            return extFile;
        }

        public string GetFileTypeID()
        {
            return fileTypeID;
        }

        public void SetFileTypeID(string newID)
        {
            XmlNode ft = sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']");
            ft.InnerText = newID;

            this.fileTypeID = newID;
        }
        public void SetExternalFile(string extFile)
        {
            try
            {
                XmlNode ef = sdlxliff.SelectSingleNode("//*[local-name() = 'external-file']").Attributes["href"];
                ef.InnerText = extFile;

                this.externalFile = extFile;
            }
            catch (Exception extFileSetExc)
            {
                Console.WriteLine(extFileSetExc.ToString());
                this.externalFile = "";
            }

            
        }
        public void SetFilePath(string newFilePath)
        {
            this.filePath = newFilePath;
        }
        public void SetSrcLang(string lang)
        {
            try
            {
                this.srcLang = lang;
                sdlxliff.SelectSingleNode("//*[local-name() = 'file']").Attributes["source-language"].InnerText = lang;
            }
            catch (Exception srcLangExc)
            {
                Console.WriteLine("This file probably doesn't have src lang");
                this.srcLang = "";
            }
        }
        public void SetTgtLang(string lang)
        {
            try
            {
                this.tgtLang = lang;
                sdlxliff.SelectSingleNode("//*[local-name() = 'file']").Attributes["target-language"].InnerText = lang;
            }
            catch (Exception tgtLangExc)
            {
                Console.WriteLine("This file probably doesn't have tgt lang");
                this.tgtLang = "";
            }
        }

        public string GetSrcLang()
        {
            return srcLang;
        }
        public string GetTgtLang()
        {
            return tgtLang;
        }
        public XmlDocument GetXmlDocument()
        {
            return sdlxliff;
        }
        public string GetFileName()
        {
            return fileName;
        }
        public string GetFilePath()
        {
            return filePath;
        }
        public string GetOriginalFilePath()
        {
            return originalFilePath;
        }

        private void SetSrcSdlxliff(Sdlxliff file)
        {
            this.srcSdlxliff = file;
        }
        private string GetSrcExtension()
        {

            string ext = sdlxliff.SelectSingleNode("//*[local-name() = 'file']").Attributes["original"].InnerText;
            if (ext == "" || ext == "tmp")
            {
                ext = sdlxliff.SelectSingleNode("//*[local-name() = 'ref-file']").Attributes["name"].InnerText;
                if (ext == "" || ext == "tmp")
                {
                    ext = sdlxliff.SelectSingleNode("//*[local-name() = 'ref-file']").Attributes["o-path"].InnerText;
                    if (ext == "" || ext == "tmp")
                    {
                        ext = sdlxliff.SelectSingleNode("//*[local-name() = 'value'][@key='SDL:OriginalFilePath']").InnerText;
                        if (ext == "" || ext == "tmp")
                        {
                            // file type not recognized!
                        }
                    }
                }
            }

            if (ext != "") ext = Path.GetExtension(ext);

            return ext;
        }
        
        public string GuessFT()
        {
            string ext = GetSrcExtension();

            ext = ext.ToLower();
            ext = ext.Substring(1);
            string ft = "";

            switch (ext)
            {
                case "xlsx":
                    ft = "Excel 2007 v 2.0.0.0";
                    break;
                case "xls":
                    ft = "Excel 2003 v 2.0.0.0";
                    break;
                case "docx":
                    ft = "Word 2007 v 2.0.0.0";
                    break;
                case "doc":
                    ft = "Word 2000-2003 v 1.0.0.0";
                    break;
                case "pptx":
                    ft = "PowerPoint 2007 v 2.0.0.0";
                    break;
                case "ppt":
                    ft = "PowerPoint XP-2003 v 1.0.0.0";
                    break;
                case "inx":
                    ft = "Inx 1.0.0.0";
                    break;
                case "idml":
                    ft = "IDML v 1.0.0.0";
                    break;
                case "icml":
                    ft = "ICML Filter 1.0.0.0";
                    break;
                case "rtf":
                    ft = "RTF v 1.0.0.0";
                    break;
                case "mif":
                    ft = "FrameMaker 8.0 v 2.0.0.0";
                    break;
                case "odt":
                    ft = "Odt 1.0.0.0";
                    break;
                case "ott":
                    ft = "Odt 1.0.0.0";
                    break;
                case "odm":
                    ft = "Odt 1.0.0.0";
                    break;
                case "odp":
                    ft = "Odp 1.0.0.0";
                    break;
                case "otp":
                    ft = "Odp 1.0.0.0";
                    break;
                case "ods":
                    ft = "Ods 1.0.0.0";
                    break;
                case "ots":
                    ft = "Ods 1.0.0.0";
                    break;
                case "xtg":
                    ft = "QuarkXPress v 2.0.0.0";
                    break;
                case "tag":
                    ft = "QuarkXPress v 2.0.0.0";
                    break;
                case "xlf":
                    ft = "XLIFF 1.1-1.2 v 2.0.0.0";
                    break;
                case "xliff":
                    ft = "XLIFF 1.1-1.2 v 2.0.0.0";
                    break;
                case "xlz":
                    ft = "XLIFF 1.1-1.2 v 2.0.0.0";
                    break;
                case "mqxlf":
                    ft = "MemoQ v 1.0.0.0";
                    break;
                case "mqxliff":
                    ft = "MemoQ v 1.0.0.0";
                    break;
                case "mqxlz":
                    ft = "MemoQ v 1.0.0.0";
                    break;
                case "pdf":
                    ft = "PDF v 2.0.0.0";
                    break;
                case "csv":
                    ft = "CSV v 2.0.0.0";
                    break;
                case "properties":
                    ft = "Java Resources v 2.0.0.0";
                    break;
                case "po":
                    ft = "PO files v 1.0.0.0";
                    break;
                case "srt":
                    ft = "SubRip v 1.0.0.0";
                    break;
                case "resx":
                    ft = "XML: RESX v 1.2.0.0";
                    break;
                case "its":
                    ft = "XML: ITS 1.0 v 1.2.0.0";
                    break;
                case "txt":
                    ft = "Plain Text v 1.0.0.0";
                    break;
                case "ttx":
                    ft = "TTX 2.0 v 2.0.0.0";
                    break;
                case "itd":
                    ft = "ITD v 1.0.0.0";
                    break;
                default:
                    ft = "";
                    break;
            }

            return ft;
        }

        public void SaveBackup()
        {
            if (Settings.AUTO_BACKUP)
            {
                try
                {
                    if (!File.Exists(filePath + ".backup"))
                    {
                        File.Copy(filePath, filePath + ".backup");
                    }
                }
                catch (Exception backupSaveException)
                {
                    Console.WriteLine(backupSaveException.ToString());
                }
            }
        }
        public string GetBackupPath()
        {
            if (File.Exists(filePath + ".backup"))
                return filePath + ".backup";
            else
                return null;

        }
        public void Save()
        {
            using (var textWriter = new StreamWriter(filePath))
            using (var xmlWriter = new XmlWriterEntities(textWriter))
            sdlxliff.Save(xmlWriter);
        }
        public void Rename(string newName)
        {
            if (File.Exists(newName))
            {
                throw new System.IO.IOException("File already exists");
            }
            else
            {
                File.Move(filePath, newName);
                filePath = newName;
                fileName = Path.GetFileName(newName);
            }
        }
    }
}
