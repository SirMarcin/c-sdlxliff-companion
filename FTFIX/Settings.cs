﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTFIX
{
    public static class Settings
    {
        public static bool AUTO_BACKUP;
        public static bool DEPENDENCY_FIX;
        public static bool DEPENDENCY_FIX_ORIG_FILE;
        public static bool UNTASK_SUFFIX;
        public static bool UNTASK_AUTO;
        public static bool LANG_CHANGE;
    }
}
