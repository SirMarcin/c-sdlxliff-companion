﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FTFIX
{
    class XmlNameValue
    {
        private XmlDocument doc;
        private XmlElement rootElement;
        string xmlPath;
        public bool settings_autoBackup { get; private set; }
        public bool settings_fixDepencency { get; private set; }
        public bool settings_fixDepencencyOrigFile { get; private set; }
        public bool settings_untaskSuffix { get; private set; }
        public bool settings_untaskAuto { get; private set; }
        public bool settings_langChange { get; private set; }

        public XmlNameValue()
        {
            this.doc = new XmlDocument();
            this.doc.PreserveWhitespace = false;
            this.doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));
            this.rootElement = (XmlElement)doc.AppendChild(doc.CreateElement("filetypes"));
            //this.xmlPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\filetypes.xml";
            this.xmlPath = GetDefaultName();
            this.settings_autoBackup = true;
            this.SetDependencyFix(true);
            this.SetUntaskSuffix(true);
            this.SetUntaskAuto(false);
            this.SetDependencyOrigFile(false);
            this.SetLangChange(false);

            this.FillDefaultXML();
            this.SaveAsXML();
        }

        public XmlNameValue(Dictionary<string, string> newContent)
        {
            this.doc = new XmlDocument();
            this.doc.PreserveWhitespace = false;
            this.doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));
            this.rootElement = (XmlElement)doc.AppendChild(doc.CreateElement("filetypes"));
            //this.xmlPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\filetypes.xml";
            this.xmlPath = GetDefaultName();
            this.settings_autoBackup = true;
            this.SetDependencyFix(true);
            this.SetUntaskSuffix(true);
            this.SetUntaskAuto(false);
            this.SetDependencyOrigFile(false);
            this.SetLangChange(false);

            foreach (var item in newContent)
            {
                this.AddTypeXML(item.Key, item.Value);
            }

            this.SaveAsXML();
        }

        public XmlNameValue(string path)
        {
            this.doc = new XmlDocument();
            this.doc.PreserveWhitespace = false;
            this.xmlPath = path;
            doc.Load(path);
            this.rootElement = (XmlElement)doc.SelectSingleNode("//filetypes");

            try
            {
                this.settings_autoBackup = Convert.ToBoolean(rootElement.Attributes["autoBackup"].InnerText);
            }
            catch (Exception noAutoBackupValueExc)
            {
                Console.WriteLine("No auto backup settings found in XML, reverting to default (enabled).");
                this.SetAutoBackup(true);
            }

            try
            {
                this.settings_fixDepencency = Convert.ToBoolean(rootElement.Attributes["dependencyFix"].InnerText);
            }
            catch (Exception noDependencyFixValueExc)
            {
                Console.WriteLine("No auto backup settings found in XML, reverting to default (enabled).");
                this.SetDependencyFix(true);
            }

            try
            {
                this.settings_untaskSuffix = Convert.ToBoolean(rootElement.Attributes["untaskSuffix"].InnerText);
            }
            catch (Exception noUntaskSuffixValueExc)
            {
                Console.WriteLine("No untask suffix settings found in XML, reverting to default (enabled).");
                this.SetUntaskSuffix(true);
            }

            try
            {
                this.settings_untaskAuto = Convert.ToBoolean(rootElement.Attributes["untaskAuto"].InnerText);
            }
            catch (Exception noUntaskAutoValueExc)
            {
                Console.WriteLine("No untask auto settings found in XML, reverting to default (enabled).");
                this.SetUntaskAuto(false);
            }

            try
            {
                this.settings_fixDepencencyOrigFile = Convert.ToBoolean(rootElement.Attributes["dependencyFixOrigFile"].InnerText);
            }
            catch (Exception noDepOrigFileExc)
            {
                Console.WriteLine("No dependency orig file settings found in XML, reverting to default (enabled).");
                this.SetDependencyOrigFile(false);
            }

            try
            {
                this.settings_langChange = Convert.ToBoolean(rootElement.Attributes["langChange"].InnerText);
            }
            catch (Exception noDepOrigFileExc)
            {
                Console.WriteLine("No lang change settings found in XML, reverting to default (enabled).");
                this.SetLangChange(false);
            }

        }

        private string GetDefaultName()
        {
            string name = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            name += "\\FTFIX_filetypes.xml";

            return name;
        }

        public void SaveAsXML()
        {
            doc.Save(xmlPath);
        }

        private void FillDefaultXML()
        {
            AddTypeXML("Excel 2007-2013", "Excel 2007 v 2.0.0.0");
            AddTypeXML("Excel 2003", "Excel 2003 v 2.0.0.0");
            AddTypeXML("Excel Bilingual (Bilingual Excel v 1.0.0.0)", "Bilingual Excel v 1.0.0.0");
            AddTypeXML("CSV", "CSV v 2.0.0.0");
            AddTypeXML("Word 2007-2013", "Word 2007 v 2.0.0.0");
            AddTypeXML("Word 2007-2016 (WordprocessingML v. 2)", "WordprocessingML v. 2");
            AddTypeXML("Word 2003", "Word 2000-2003 v 1.0.0.0");
            AddTypeXML("Word (rtf)", "RTF v 1.0.0.0");
            AddTypeXML("PowerPoint 2007-2013", "PowerPoint 2007 v 2.0.0.0");
            AddTypeXML("PowerPoint 2003", "PowerPoint XP-2003 v 1.0.0.0");
            AddTypeXML("InDesign (inx)", "Inx 1.0.0.0");
            AddTypeXML("InDesign (idml)", "IDML v 1.0.0.0");
            AddTypeXML("InDesign (icml)", "ICML Filter 1.0.0.0");
            AddTypeXML("FrameMaker", "FrameMaker 8.0 v 2.0.0.0");
            AddTypeXML("OpenOffice (odt)", "Odt 1.0.0.0");
            AddTypeXML("OpenOffice (odp)", "Odp 1.0.0.0");
            AddTypeXML("OpenOffice (ods)", "Ods 1.0.0.0");
            AddTypeXML("QuarkXPress", "QuarkXPress v 2.0.0.0");
            AddTypeXML("XLIFF", "XLIFF 1.1-1.2 v 2.0.0.0");
            AddTypeXML("MemoQ", "MemoQ v 1.0.0.0");
            AddTypeXML("PDF", "PDF v 2.0.0.0");
            AddTypeXML("Java (properties)", "Java Resources v 2.0.0.0");
            AddTypeXML("Portable Object", "PO files v 1.0.0.0");
            AddTypeXML("SubRip", "SubRip v 1.0.0.0");
            AddTypeXML(".NET (resx)", "XML: RESX v 1.2.0.0");
            AddTypeXML("ITS", "XML: ITS 1.0 v 1.2.0.0");
            AddTypeXML("Plain Text (txt)", "Plain Text v 1.0.0.0");
            AddTypeXML("TTX", "TTX 2.0 v 2.0.0.0");
            AddTypeXML("ITD", "ITD v 1.0.0.0");
            //AddTypeXML("--- CLEAR ---", "");
        }

        public Dictionary<string, string> LoadFromXML()
        {
            Dictionary<string, string> types = new Dictionary<string, string>();

            XmlNodeList elements = doc.SelectNodes("//type");

            Console.WriteLine("Loading from XML");

            foreach (XmlNode node in elements)
            {
                if (!types.ContainsKey(node.Attributes["name"].InnerText))
                    types.Add(node.Attributes["name"].InnerText, node.InnerText);
            }

            return types;
        }

        public Dictionary<string, string> LoadFromXML(string path)
        {
            Dictionary<string, string> types = new Dictionary<string, string>();
            XmlDocument tempDoc = new XmlDocument();
            tempDoc.Load(path);

            XmlNodeList elements = tempDoc.SelectNodes("//type");

            foreach (XmlNode node in elements)
            {
                if (!types.ContainsKey(node.Attributes["name"].InnerText))
                    types.Add(node.Attributes["name"].InnerText, node.InnerText);
            }

            return types;
        } 

        public void AddTypeXML(string att, string val)
        {
            XmlElement type = (XmlElement)rootElement.AppendChild(doc.CreateElement("type"));
            type.InnerText = val;
            type.SetAttribute("name", att);
        }
        public void SetAutoBackup(bool option)
        {
            this.settings_autoBackup = option;

            Console.WriteLine("Setting auto backup to " + option.ToString());

            rootElement.SetAttribute("autoBackup", option.ToString());

            SaveAsXML();
        }
        public void SetDependencyFix(bool option)
        {
            this.settings_fixDepencency = option;

            Console.WriteLine("Setting dependency fix to " + option.ToString());

            rootElement.SetAttribute("dependencyFix", option.ToString());

            SaveAsXML();
        }
        public void SetUntaskSuffix(bool option)
        {
            this.settings_untaskSuffix = option;

            Console.WriteLine("Setting untask suffix to " + option.ToString());

            rootElement.SetAttribute("untaskSuffix", option.ToString());

            SaveAsXML();
        }
        public void SetUntaskAuto(bool option)
        {
            this.settings_untaskAuto = option;

            Console.WriteLine("Setting untask auto to " + option.ToString());

            rootElement.SetAttribute("untaskAuto", option.ToString());

            SaveAsXML();
        }
        public void SetDependencyOrigFile(bool option)
        {
            this.settings_fixDepencencyOrigFile = option;

            Console.WriteLine("Setting dependency orig file to " + option.ToString());

            rootElement.SetAttribute("dependencyFixOrigFile", option.ToString());

            SaveAsXML();
        }
        public void SetLangChange(bool option)
        {
            this.settings_langChange = option;

            Console.WriteLine("Setting visibility of lang text boxes to " + option.ToString());

            rootElement.SetAttribute("langChange", option.ToString());

            SaveAsXML();
        }

        public string GetXMLPath()
        {
            return xmlPath;
        }
    }
}
