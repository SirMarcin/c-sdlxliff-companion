﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FTFIX
{
    public partial class FTSettings : Form
    {
        private int sortColumn = -1;
        private XmlNameValue filetypesXML;
        private Dictionary<string, string> filetypesDict;
        private Form1 mainForm;
        private string xmlPath;
        private bool modified;


        private void FTSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modified)
            {
                var closeNotSavedResponse = MessageBox.Show("All unsaved changes will be lost. Do you want to save them before closing?", "Save changes", MessageBoxButtons.YesNoCancel);

                if (closeNotSavedResponse == DialogResult.Yes)
                {
                    SaveItems();
                }
                else if (closeNotSavedResponse == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        public FTSettings(string xmlPath)
        {
            Console.WriteLine("From FTSettings: " + xmlPath);

            InitializeComponent();

            filetypesXML = new XmlNameValue(xmlPath);

            this.xmlPath = xmlPath;

            this.InitializeListView();
            this.LoadTypes();
            this.FormClosing += FTSettings_FormClosing;
            this.KeyDown += FTSettings_KeyDown;

            modified = false;

            checkBoxAutoBackup.CheckedChanged +=checkBoxAutoBackup_CheckedChanged;
            checkBoxFixDepencency.CheckedChanged += checkBoxFixDepencency_CheckedChanged;
            checkBoxSuffix.CheckedChanged += checkBoxSuffix_CheckedChanged;
            checkBoxAutoUntask.CheckedChanged += checkBoxAutoUntask_CheckedChanged;
            checkBoxFixDepencencyOrigFile.CheckedChanged += checkBoxFixDepencencyOrigFile_CheckedChanged;
            checkBoxLangChange.CheckedChanged += checkBoxLangChange_CheckedChanged;

            //checkBoxAutoBackup.Checked = filetypesXML.settings_autoBackup;
            checkBoxAutoBackup.Checked = Settings.AUTO_BACKUP;
            checkBoxFixDepencency.Checked = Settings.DEPENDENCY_FIX;
            checkBoxFixDepencencyOrigFile.Checked = Settings.DEPENDENCY_FIX_ORIG_FILE;
            checkBoxSuffix.Checked = Settings.UNTASK_SUFFIX;
            checkBoxAutoUntask.Checked = Settings.UNTASK_AUTO;
            checkBoxLangChange.Checked = Settings.LANG_CHANGE;

            if (Settings.UNTASK_SUFFIX)
                labelAfterValue.Text = "Example-file-name.docx.sdlxliff";
            else
                labelAfterValue.Text = "Example-file-name.docx_docx.sdlxliff";

            if (Settings.DEPENDENCY_FIX)
                checkBoxFixDepencencyOrigFile.Enabled = true;
            else
                checkBoxFixDepencencyOrigFile.Enabled = false;

        }

        void checkBoxLangChange_CheckedChanged(object sender, EventArgs e)
        {
            bool langChange = checkBoxLangChange.Checked;

            filetypesXML.SetLangChange(langChange);
            Settings.LANG_CHANGE = langChange;
            try
            {
                mainForm.ToggleLanguageFields(langChange);
            }
            catch (NullReferenceException langChangeExc)
            {
                Console.WriteLine("Can't change the visibility of parent control because it hasn't been initialized yet");
            }
        }

        void checkBoxFixDepencencyOrigFile_CheckedChanged(object sender, EventArgs e)
        {
            bool depOrigFile = checkBoxFixDepencencyOrigFile.Checked;

            filetypesXML.SetDependencyOrigFile(depOrigFile);
            Settings.DEPENDENCY_FIX_ORIG_FILE = depOrigFile;
        }

        void checkBoxAutoUntask_CheckedChanged(object sender, EventArgs e)
        {
            bool untaskAuto = checkBoxAutoUntask.Checked;

            filetypesXML.SetUntaskAuto(untaskAuto);
            Settings.UNTASK_AUTO = untaskAuto;
        }

        void checkBoxSuffix_CheckedChanged(object sender, EventArgs e)
        {
            bool untaskSuffix = checkBoxSuffix.Checked;

            if (untaskSuffix)
                labelAfterValue.Text = "Example-file-name.docx.sdlxliff";
            else
                labelAfterValue.Text = "Example-file-name.docx_docx.sdlxliff";

            filetypesXML.SetUntaskSuffix(untaskSuffix);
            Settings.UNTASK_SUFFIX = untaskSuffix;
        }

        void checkBoxFixDepencency_CheckedChanged(object sender, EventArgs e)
        {
            bool dependencyFix = checkBoxFixDepencency.Checked;

            if (dependencyFix)
            {
                checkBoxFixDepencencyOrigFile.Enabled = true;
            }
            else
            {
                checkBoxFixDepencencyOrigFile.Enabled = false;
            }

            filetypesXML.SetDependencyFix(dependencyFix);
            Settings.DEPENDENCY_FIX = dependencyFix;
        }

        void FTSettings_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void checkBoxAutoBackup_CheckedChanged(object sender, EventArgs e)
        {
            bool autoBackup = checkBoxAutoBackup.Checked;

            filetypesXML.SetAutoBackup(autoBackup);
            Settings.AUTO_BACKUP = autoBackup;
        }

        private void InitializeListView()
        {
            listView1.Columns.Add("File Type Name", 170);
            listView1.Columns.Add("File Type ID", 170);
            listView1.View = View.Details;
            listView1.SelectedIndexChanged += listView1_SelectedIndexChanged;
            listView1.ColumnClick += listView1_ColumnClick;
        }

        private void LoadTypes()
        {
            filetypesDict = filetypesXML.LoadFromXML();

            foreach (var type in filetypesDict)
            {
                string name, value;

                name = type.Key;
                value = type.Value;

                ListViewAddItem(name, value);
            }
        }

        private void LoadTypes(Dictionary<string, string> tempTypes)
        {
            foreach (var type in tempTypes)
            {
                string name, value;

                name = type.Key;
                value = type.Value;

                ListViewAddItem(name, value);
            }
        }

        private void ListViewAddItem(string name, string value)
        {
            ListViewItem item = new ListViewItem(new[] { name, value });
            listView1.Items.Add(item);
        }

        private void ListViewFillWithDefaults()
        {
            ListViewAddItem("Excel 2007-2013", "Excel 2007 v 2.0.0.0");
            ListViewAddItem("Excel 2003", "Excel 2003 v 2.0.0.0");
            ListViewAddItem("Excel Bilingual (Bilingual Excel v 1.0.0.0)", "Bilingual Excel v 1.0.0.0");
            ListViewAddItem("CSV", "CSV v 2.0.0.0");
            ListViewAddItem("Word 2007-2013", "Word 2007 v 2.0.0.0");
            ListViewAddItem("Word 2007-2016 (WordprocessingML v. 2)", "WordprocessingML v. 2");
            ListViewAddItem("Word 2003", "Word 2000-2003 v 1.0.0.0");
            ListViewAddItem("Word (rtf)", "RTF v 1.0.0.0");
            ListViewAddItem("PowerPoint 2007-2013", "PowerPoint 2007 v 2.0.0.0");
            ListViewAddItem("PowerPoint 2003", "PowerPoint XP-2003 v 1.0.0.0");
            ListViewAddItem("InDesign (inx)", "Inx 1.0.0.0");
            ListViewAddItem("InDesign (idml)", "IDML v 1.0.0.0");
            ListViewAddItem("InDesign (icml)", "ICML Filter 1.0.0.0");
            ListViewAddItem("FrameMaker", "FrameMaker 8.0 v 2.0.0.0");
            ListViewAddItem("OpenOffice (odt)", "Odt 1.0.0.0");
            ListViewAddItem("OpenOffice (odp)", "Odp 1.0.0.0");
            ListViewAddItem("OpenOffice (ods)", "Ods 1.0.0.0");
            ListViewAddItem("QuarkXPress", "QuarkXPress v 2.0.0.0");
            ListViewAddItem("XLIFF", "XLIFF 1.1-1.2 v 2.0.0.0");
            ListViewAddItem("MemoQ", "MemoQ v 1.0.0.0");
            ListViewAddItem("PDF", "PDF v 2.0.0.0");
            ListViewAddItem("Java (properties)", "Java Resources v 2.0.0.0");
            ListViewAddItem("Portable Object", "PO files v 1.0.0.0");
            ListViewAddItem("SubRip", "SubRip v 1.0.0.0");
            ListViewAddItem(".NET (resx)", "XML: RESX v 1.2.0.0");
            ListViewAddItem("ITS", "XML: ITS 1.0 v 1.2.0.0");
            ListViewAddItem("Plain Text (txt)", "Plain Text v 1.0.0.0");
            ListViewAddItem("TTX", "TTX 2.0 v 2.0.0.0");
            ListViewAddItem("ITD", "ITD v 1.0.0.0");
        }

        private void SaveItems()
        {
            Dictionary<string, string> newItems = new Dictionary<string, string>();

            foreach (ListViewItem item in this.listView1.Items)
            {
                string key = item.SubItems[0].Text;
                string value = item.SubItems[1].Text;
                if (!newItems.ContainsKey(key))
                    newItems.Add(key, value);
            }

            filetypesXML = new XmlNameValue(newItems);

            this.SetStatusLabel("Saved");

            mainForm.SetComboBox(newItems);

            modified = false;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != sortColumn)
            {
                // Set the sort column to the new column.
                sortColumn = e.Column;
                // Set the sort order to ascending by default.
                listView1.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (listView1.Sorting == SortOrder.Ascending)
                    listView1.Sorting = SortOrder.Descending;
                else
                    listView1.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            listView1.Sort();
            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            this.listView1.ListViewItemSorter = new ListViewItemComparer(e.Column,
                                                              listView1.Sorting);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            // TO BE DONE
            string name = "";
            string value ="";

            name = Microsoft.VisualBasic.Interaction.InputBox("File Type Name", "Name").Trim();

            if (name != "")
                value = Microsoft.VisualBasic.Interaction.InputBox("File Type ID", "ID").Trim();

            if (name != "" && value != "")
            {
                ListViewAddItem(name, value);
                this.SetStatusLabel("Added new file type");

                modified = true;
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection items = this.listView1.SelectedItems;
            int deletedCount = 0;

            foreach (ListViewItem item in items){
                item.Remove();
                deletedCount++;
            }

            this.SetStatusLabel("Deleted file types: " + deletedCount);

            modified = true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            this.SaveItems();
        }

        private void ListViewClear()
        {
            foreach (ListViewItem item in this.listView1.Items)
            {
                item.Remove();
            }
        }

        private void buttonDefault_Click(object sender, EventArgs e)
        {
            ListViewClear();
            
            //filetypesXML = new XmlNameValue();
            //LoadTypes();

            this.ListViewFillWithDefaults();
            this.SetStatusLabel("Default file types loaded");

            modified = true;
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Multiselect = false;
            string importFilePath = "";

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                importFilePath = openFile.FileName;
                Console.WriteLine(importFilePath);

                //ImportSettingsFile(importFilePath);

                ListViewClear();

                //filetypesXML = new XmlNameValue(xmlPath);
                Dictionary<string, string> newItems = filetypesXML.LoadFromXML(importFilePath);
                this.LoadTypes(newItems);
                
                //mainForm.SetComboBox(newItems);
            }
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveAs = new SaveFileDialog();
            saveAs.AddExtension = true;
            saveAs.Filter = "FTFIX xml settings file|*.xml";

           if (modified)
                {
                    var saveAsResponse = MessageBox.Show("Changes have not been saved. Do you want to save them before export?", "Save changes", MessageBoxButtons.YesNoCancel);

                    if (saveAsResponse == DialogResult.Yes)
                    {
                        if (saveAs.ShowDialog() == DialogResult.OK)
                        {

                            this.SaveItems();

                            string exportFilePath = saveAs.FileName;
                            ExportSettingsFile(exportFilePath);


                            this.SetStatusLabel("Exported");
                        }
                        else
                        {
                            this.SetStatusLabel("Export aborted");
                        }
                    }
                    else if (saveAsResponse == DialogResult.No)
                    {
                        if (saveAs.ShowDialog() == DialogResult.OK)
                        {
                            string exportFilePath = saveAs.FileName;
                            ExportSettingsFile(exportFilePath);

                            this.SetStatusLabel("Exported");
                        }
                        else
                        {
                            this.SetStatusLabel("Export aborted");
                        }
                    }
                    else
                    {
                        this.SetStatusLabel("Export aborted");
                    }
                }
                else
                {
                    if (saveAs.ShowDialog() == DialogResult.OK)
                    {
                        string exportFilePath = saveAs.FileName;
                        ExportSettingsFile(exportFilePath);

                        this.SetStatusLabel("Exported");
                    }
                }
            
        }

        private void ExportSettingsFile(string path)
        {
            File.Copy(xmlPath, path, true);
            modified = false;

            SetStatusLabel("Exported");
        }

        private void ImportSettingsFile(string importFilePath)
        {
            File.Copy(importFilePath, xmlPath, true);

            SetStatusLabel("Imported");
        }

        private void SetStatusLabel(string status)
        {
            this.labelStatus.Text = status;
        }

        public void SetMainForm(Form1 form)
        {
            mainForm = form;
            Console.WriteLine("Main form set");
        }
    }
}
