﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTFIX
{
    public static class FileList
    {
        public static List<SdlxliffDetails> TARGET_FILES = new List<SdlxliffDetails>();
        public static List<SdlxliffDetails> SOURCE_FILES = new List<SdlxliffDetails>();

        public static void PrintSourceFiles()
        {
            foreach (SdlxliffDetails details in FileList.SOURCE_FILES)
            {
                Console.WriteLine(details.fileName);
                Console.WriteLine(details.filePath);
                Console.WriteLine(details.fileTypeID);
                Console.WriteLine(details.originalFilePath);
                Console.WriteLine(details.srcLang);
                Console.WriteLine(details.tgtLang);
                Console.WriteLine();
            }
        }
        public static void PrintTargetFiles()
        {
            foreach (SdlxliffDetails details in FileList.TARGET_FILES)
            {
                Console.WriteLine(details.fileName);
                Console.WriteLine(details.filePath);
                Console.WriteLine(details.fileTypeID);
                Console.WriteLine(details.originalFilePath);
                Console.WriteLine(details.srcLang);
                Console.WriteLine(details.tgtLang);
                Console.WriteLine();
            }
        }

        public static void CleanSourceList()
        {
            SOURCE_FILES.Clear();
        }
        public static void CleanTargetList()
        {
            TARGET_FILES.Clear();
        }

        public static void RemoveTargetItem(string filePath)
        {
            int index = 0;
            foreach (SdlxliffDetails target in TARGET_FILES)
            {
                
                if (target.filePath == filePath)
                {
                    TARGET_FILES.RemoveAt(index);
                    break;
                }
                else
                    index++;

            }
        }
        public static void RemoveSourceItem(string filePath)
        {
            int index = 0;
            foreach (SdlxliffDetails source in SOURCE_FILES)
            {
                if (source.filePath == filePath)
                {
                    SOURCE_FILES.RemoveAt(index);
                    break;
                }
                else
                    index++;
            }
        }

        public static SdlxliffDetails GetTargetFile(String targetFilePath)
        {
            SdlxliffDetails target = null;

            foreach (SdlxliffDetails file in TARGET_FILES)
            {
                if (file.filePath == targetFilePath)
                {
                    target = file;
                    break;
                }
            }


            return target;

        }
        public static SdlxliffDetails GetSourceFile(String sourceFilePath)
        {
            SdlxliffDetails source = null;

            foreach (SdlxliffDetails file in SOURCE_FILES)
            {
                if (file.filePath == sourceFilePath)
                {
                    source = file;
                    break;
                }
            }


            return source;
        }
    }
}
