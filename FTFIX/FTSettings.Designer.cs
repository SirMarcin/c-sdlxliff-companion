﻿namespace FTFIX
{
    partial class FTSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FTSettings));
            this.listView1 = new System.Windows.Forms.ListView();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonExport = new System.Windows.Forms.Button();
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.checkBoxAutoBackup = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBoxMissingDepFile = new System.Windows.Forms.GroupBox();
            this.checkBoxFixDepencencyOrigFile = new System.Windows.Forms.CheckBox();
            this.checkBoxFixDepencency = new System.Windows.Forms.CheckBox();
            this.groupBoxUnTASK = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxAutoUntask = new System.Windows.Forms.CheckBox();
            this.labelAfterValue = new System.Windows.Forms.Label();
            this.labelBeforeValue = new System.Windows.Forms.Label();
            this.labelAfter = new System.Windows.Forms.Label();
            this.labelBefore = new System.Windows.Forms.Label();
            this.checkBoxSuffix = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxCommonSett = new System.Windows.Forms.GroupBox();
            this.checkBoxLangChange = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBoxMissingDepFile.SuspendLayout();
            this.groupBoxUnTASK.SuspendLayout();
            this.groupBoxCommonSett.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.LabelEdit = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(364, 461);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(371, 459);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(82, 23);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(371, 95);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(83, 83);
            this.buttonDelete.TabIndex = 2;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Location = new System.Drawing.Point(370, 6);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(83, 83);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonExport
            // 
            this.buttonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExport.Location = new System.Drawing.Point(371, 430);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(82, 23);
            this.buttonExport.TabIndex = 4;
            this.buttonExport.Text = "Export";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImport.Location = new System.Drawing.Point(371, 401);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(82, 23);
            this.buttonImport.TabIndex = 5;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonDefault
            // 
            this.buttonDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDefault.Location = new System.Drawing.Point(371, 184);
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.Size = new System.Drawing.Size(83, 23);
            this.buttonDefault.TabIndex = 6;
            this.buttonDefault.Text = "Default";
            this.buttonDefault.UseVisualStyleBackColor = true;
            this.buttonDefault.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(6, 469);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(86, 13);
            this.labelStatus.TabIndex = 7;
            this.labelStatus.Text = "File types loaded";
            // 
            // checkBoxAutoBackup
            // 
            this.checkBoxAutoBackup.AutoSize = true;
            this.checkBoxAutoBackup.Location = new System.Drawing.Point(19, 32);
            this.checkBoxAutoBackup.Name = "checkBoxAutoBackup";
            this.checkBoxAutoBackup.Size = new System.Drawing.Size(87, 17);
            this.checkBoxAutoBackup.TabIndex = 0;
            this.checkBoxAutoBackup.Text = "Auto-backup";
            this.checkBoxAutoBackup.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(7, 6);
            this.tabControl1.MinimumSize = new System.Drawing.Size(468, 486);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(468, 514);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.listView1);
            this.tabPage1.Controls.Add(this.labelStatus);
            this.tabPage1.Controls.Add(this.buttonAdd);
            this.tabPage1.Controls.Add(this.buttonImport);
            this.tabPage1.Controls.Add(this.buttonExport);
            this.tabPage1.Controls.Add(this.buttonDefault);
            this.tabPage1.Controls.Add(this.buttonSave);
            this.tabPage1.Controls.Add(this.buttonDelete);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(460, 488);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "File Type IDs";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxMissingDepFile);
            this.tabPage2.Controls.Add(this.groupBoxUnTASK);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.groupBoxCommonSett);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(460, 488);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Options";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBoxMissingDepFile
            // 
            this.groupBoxMissingDepFile.Controls.Add(this.checkBoxFixDepencencyOrigFile);
            this.groupBoxMissingDepFile.Controls.Add(this.checkBoxFixDepencency);
            this.groupBoxMissingDepFile.Location = new System.Drawing.Point(6, 108);
            this.groupBoxMissingDepFile.Name = "groupBoxMissingDepFile";
            this.groupBoxMissingDepFile.Size = new System.Drawing.Size(448, 100);
            this.groupBoxMissingDepFile.TabIndex = 5;
            this.groupBoxMissingDepFile.TabStop = false;
            this.groupBoxMissingDepFile.Text = "Missing dependency file";
            // 
            // checkBoxFixDepencencyOrigFile
            // 
            this.checkBoxFixDepencencyOrigFile.AutoSize = true;
            this.checkBoxFixDepencencyOrigFile.Location = new System.Drawing.Point(18, 57);
            this.checkBoxFixDepencencyOrigFile.Name = "checkBoxFixDepencencyOrigFile";
            this.checkBoxFixDepencencyOrigFile.Size = new System.Drawing.Size(305, 17);
            this.checkBoxFixDepencencyOrigFile.TabIndex = 2;
            this.checkBoxFixDepencencyOrigFile.Text = "Automatically use original file path even if it\'s not accessible";
            this.checkBoxFixDepencencyOrigFile.UseVisualStyleBackColor = true;
            // 
            // checkBoxFixDepencency
            // 
            this.checkBoxFixDepencency.AutoSize = true;
            this.checkBoxFixDepencency.Location = new System.Drawing.Point(18, 33);
            this.checkBoxFixDepencency.Name = "checkBoxFixDepencency";
            this.checkBoxFixDepencency.Size = new System.Drawing.Size(202, 17);
            this.checkBoxFixDepencency.TabIndex = 1;
            this.checkBoxFixDepencency.Text = "Attempt to fix missing depencency file";
            this.checkBoxFixDepencency.UseVisualStyleBackColor = true;
            // 
            // groupBoxUnTASK
            // 
            this.groupBoxUnTASK.Controls.Add(this.label1);
            this.groupBoxUnTASK.Controls.Add(this.checkBoxAutoUntask);
            this.groupBoxUnTASK.Controls.Add(this.labelAfterValue);
            this.groupBoxUnTASK.Controls.Add(this.labelBeforeValue);
            this.groupBoxUnTASK.Controls.Add(this.labelAfter);
            this.groupBoxUnTASK.Controls.Add(this.labelBefore);
            this.groupBoxUnTASK.Controls.Add(this.checkBoxSuffix);
            this.groupBoxUnTASK.Location = new System.Drawing.Point(6, 214);
            this.groupBoxUnTASK.Name = "groupBoxUnTASK";
            this.groupBoxUnTASK.Size = new System.Drawing.Size(448, 162);
            this.groupBoxUnTASK.TabIndex = 4;
            this.groupBoxUnTASK.TabStop = false;
            this.groupBoxUnTASK.Text = "UnTASK";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(8, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(433, 2);
            this.label1.TabIndex = 0;
            // 
            // checkBoxAutoUntask
            // 
            this.checkBoxAutoUntask.AutoSize = true;
            this.checkBoxAutoUntask.Location = new System.Drawing.Point(18, 34);
            this.checkBoxAutoUntask.Name = "checkBoxAutoUntask";
            this.checkBoxAutoUntask.Size = new System.Drawing.Size(242, 17);
            this.checkBoxAutoUntask.TabIndex = 6;
            this.checkBoxAutoUntask.Text = "Run UnTASK automatically when loading files";
            this.checkBoxAutoUntask.UseVisualStyleBackColor = true;
            // 
            // labelAfterValue
            // 
            this.labelAfterValue.AutoSize = true;
            this.labelAfterValue.Location = new System.Drawing.Point(64, 123);
            this.labelAfterValue.Name = "labelAfterValue";
            this.labelAfterValue.Size = new System.Drawing.Size(178, 13);
            this.labelAfterValue.TabIndex = 5;
            this.labelAfterValue.Text = "Example-file-name.docx_docx.sdlxliff";
            // 
            // labelBeforeValue
            // 
            this.labelBeforeValue.AutoSize = true;
            this.labelBeforeValue.Location = new System.Drawing.Point(64, 99);
            this.labelBeforeValue.Name = "labelBeforeValue";
            this.labelBeforeValue.Size = new System.Drawing.Size(356, 13);
            this.labelBeforeValue.TabIndex = 4;
            this.labelBeforeValue.Text = "TASK123456_EN-US-PL_CVTITEM_Example-file-name_docx.docx.sdlxliff";
            // 
            // labelAfter
            // 
            this.labelAfter.AutoSize = true;
            this.labelAfter.Location = new System.Drawing.Point(18, 122);
            this.labelAfter.Name = "labelAfter";
            this.labelAfter.Size = new System.Drawing.Size(31, 13);
            this.labelAfter.TabIndex = 3;
            this.labelAfter.Text = "after:";
            // 
            // labelBefore
            // 
            this.labelBefore.AutoSize = true;
            this.labelBefore.Location = new System.Drawing.Point(18, 99);
            this.labelBefore.Name = "labelBefore";
            this.labelBefore.Size = new System.Drawing.Size(40, 13);
            this.labelBefore.TabIndex = 2;
            this.labelBefore.Text = "before:";
            // 
            // checkBoxSuffix
            // 
            this.checkBoxSuffix.AutoSize = true;
            this.checkBoxSuffix.Location = new System.Drawing.Point(18, 70);
            this.checkBoxSuffix.Name = "checkBoxSuffix";
            this.checkBoxSuffix.Size = new System.Drawing.Size(93, 17);
            this.checkBoxSuffix.TabIndex = 1;
            this.checkBoxSuffix.Text = "Remove suffix";
            this.checkBoxSuffix.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Location = new System.Drawing.Point(0, 382);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(459, 106);
            this.panel1.TabIndex = 3;
            // 
            // groupBoxCommonSett
            // 
            this.groupBoxCommonSett.Controls.Add(this.checkBoxLangChange);
            this.groupBoxCommonSett.Controls.Add(this.checkBoxAutoBackup);
            this.groupBoxCommonSett.Location = new System.Drawing.Point(6, 6);
            this.groupBoxCommonSett.Name = "groupBoxCommonSett";
            this.groupBoxCommonSett.Size = new System.Drawing.Size(448, 96);
            this.groupBoxCommonSett.TabIndex = 2;
            this.groupBoxCommonSett.TabStop = false;
            this.groupBoxCommonSett.Text = "Common";
            // 
            // checkBoxLangChange
            // 
            this.checkBoxLangChange.AutoSize = true;
            this.checkBoxLangChange.Location = new System.Drawing.Point(19, 56);
            this.checkBoxLangChange.Name = "checkBoxLangChange";
            this.checkBoxLangChange.Size = new System.Drawing.Size(170, 17);
            this.checkBoxLangChange.TabIndex = 1;
            this.checkBoxLangChange.Text = "Enable language modifications";
            this.checkBoxLangChange.UseVisualStyleBackColor = true;
            // 
            // FTSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 525);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FTSettings";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBoxMissingDepFile.ResumeLayout(false);
            this.groupBoxMissingDepFile.PerformLayout();
            this.groupBoxUnTASK.ResumeLayout(false);
            this.groupBoxUnTASK.PerformLayout();
            this.groupBoxCommonSett.ResumeLayout(false);
            this.groupBoxCommonSett.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Button buttonDefault;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.CheckBox checkBoxAutoBackup;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBoxCommonSett;
        private System.Windows.Forms.CheckBox checkBoxFixDepencency;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxUnTASK;
        private System.Windows.Forms.CheckBox checkBoxSuffix;
        private System.Windows.Forms.Label labelAfter;
        private System.Windows.Forms.Label labelBefore;
        private System.Windows.Forms.Label labelAfterValue;
        private System.Windows.Forms.Label labelBeforeValue;
        private System.Windows.Forms.CheckBox checkBoxAutoUntask;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxMissingDepFile;
        private System.Windows.Forms.CheckBox checkBoxFixDepencencyOrigFile;
        private System.Windows.Forms.CheckBox checkBoxLangChange;
    }
}