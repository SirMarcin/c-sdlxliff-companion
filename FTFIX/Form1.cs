﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FTFIX
{
    public partial class Form1 : Form
    {
        private int pathIndex, idIndex, statusIndex, srcIndex, tgtIndex, uPathIndex, itemLangChangeIndex;
        private int sortColumn = -1;
        private XmlNameValue fileTypes;
        private List<Items> dataSource;
        private string toolTipPrefix;

        public string xmlPath;

        public Form1()
        {
            InitializeComponent();

            pathIndex = 1;
            idIndex = 2;
            statusIndex = 3;
            srcIndex = 4;
            tgtIndex = 5;
            uPathIndex = 6;
            itemLangChangeIndex = 7;

            toolTipPrefix = "Original file path: \n";


            // > CONTEXT MENU
            ContextMenuStrip rightClick = new ContextMenuStrip();

            ToolStripMenuItem rcRemove = new ToolStripMenuItem("Remove");
            ToolStripMenuItem rcRemoveAll = new ToolStripMenuItem("Remove all");
            ToolStripMenuItem rcSelectAll = new ToolStripMenuItem("Select all");
            ToolStripMenuItem rcUntask = new ToolStripMenuItem("UnTASK");
            ToolStripMenuItem rcOpenFolder = new ToolStripMenuItem("Open original folder");
            ToolStripMenuItem rcCopyFolderPath = new ToolStripMenuItem("Copy original folder path");

            rcRemove.Click += new EventHandler(rcRemove_Click);
            rcRemoveAll.Click += new EventHandler(rcRemoveAll_Click);
            rcSelectAll.Click += new EventHandler(rcSelectAll_Click);
            rcOpenFolder.Click += new EventHandler(rcOpenFolder_Click);
            rcCopyFolderPath.Click += new EventHandler(rcCopyFolderPath_Click);
            rcUntask.Click += new EventHandler(rcUntask_Click);

            rightClick.Items.AddRange(new ToolStripItem[] { 
                rcRemove, 
                rcRemoveAll, 
                rcSelectAll, 
                new ToolStripSeparator(), 
                rcUntask,
                new ToolStripSeparator(), 
                rcOpenFolder, 
                rcCopyFolderPath });

            listView1.ContextMenuStrip = rightClick;
            listView1.ContextMenuStrip.Opening += ContextMenuStrip_Opening;

            // < CONTEXT MENU

            backgroundWorker1.DoWork += backgroundWorker1_DoWork;

            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);

            listView1.Columns.Add("", 24);
            listView1.Columns.Add("File name", 389);
            listView1.Columns.Add("File Type ID", 140);
            listView1.Columns.Add("File Type Status", 90);
            listView1.Columns.Add("SRC", 50);
            listView1.Columns.Add("TGT", 50);
            listView1.View = View.Details;
            listView1.SelectedIndexChanged += listView1_SelectedIndexChanged;
            listView1.ColumnClick += listView1_ColumnClick;
            listView1.KeyDown += listView1_KeyDown;

            ImageList dependencyIcons = new ImageList();

            dependencyIcons.Images.Add(FTFIX.Properties.Resources.dependency01);
            dependencyIcons.Images.Add(FTFIX.Properties.Resources.dependency02);
            dependencyIcons.Images.Add(FTFIX.Properties.Resources.dependency03);
            dependencyIcons.Images.Add(FTFIX.Properties.Resources.untask01);
            dependencyIcons.Images.Add(FTFIX.Properties.Resources.dep_un1);

            listView1.SmallImageList = dependencyIcons;

            textBox1.Text = "Type in a path or drag SDLXLIFF files to the window";

            //xmlPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\filetypes.xml";
            xmlPath = GetDefaultName();


            if (!File.Exists(xmlPath))
            {
                fileTypes = new XmlNameValue();
            }
            else
            {
                fileTypes = new XmlNameValue(xmlPath);
            }

            Settings.AUTO_BACKUP = fileTypes.settings_autoBackup;
            Settings.DEPENDENCY_FIX = fileTypes.settings_fixDepencency;
            Settings.UNTASK_SUFFIX = fileTypes.settings_untaskSuffix;
            Settings.UNTASK_AUTO = fileTypes.settings_untaskAuto;
            Settings.DEPENDENCY_FIX_ORIG_FILE = fileTypes.settings_fixDepencencyOrigFile;
            Settings.LANG_CHANGE = fileTypes.settings_langChange;

            dataSource = new List<Items>();

            try
            {
                Dictionary<string, string> types = fileTypes.LoadFromXML();

                foreach (var type in types)
                {
                    dataSource.Add(new Items(type.Key, type.Value));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("PROBLEM! " + e.Message);
                dataSource.Add(new Items("Excel 2007-2013", "Excel 2007 v 2.0.0.0"));
                dataSource.Add(new Items("Excel 2003", "Excel 2003 v 2.0.0.0"));
                dataSource.Add(new Items("Excel Bilingual (Bilingual Excel v 1.0.0.0)", "Bilingual Excel v 1.0.0.0"));
                dataSource.Add(new Items("CSV", "CSV v 2.0.0.0"));
                dataSource.Add(new Items("Word 2007-2013", "Word 2007 v 2.0.0.0"));
                dataSource.Add(new Items("Word 2007-2016 (WordprocessingML v. 2)", "WordprocessingML v. 2"));
                dataSource.Add(new Items("Word 2003", "Word 2000-2003 v 1.0.0.0"));
                dataSource.Add(new Items("Word (rtf)", "RTF v 1.0.0.0"));
                dataSource.Add(new Items("PowerPoint 2007-2013", "PowerPoint 2007 v 2.0.0.0"));
                dataSource.Add(new Items("PowerPoint 2003", "PowerPoint XP-2003 v 1.0.0.0"));
                dataSource.Add(new Items("InDesign (inx)", "Inx 1.0.0.0"));
                dataSource.Add(new Items("InDesign (idml)", "IDML v 1.0.0.0"));
                dataSource.Add(new Items("InDesign (icml)", "ICML Filter 1.0.0.0"));
                dataSource.Add(new Items("FrameMaker", "FrameMaker 8.0 v 2.0.0.0"));
                dataSource.Add(new Items("OpenOffice (odt)", "Odt 1.0.0.0"));
                dataSource.Add(new Items("OpenOffice (odp)", "Odp 1.0.0.0"));
                dataSource.Add(new Items("OpenOffice (ods)", "Ods 1.0.0.0"));
                dataSource.Add(new Items("QuarkXPress", "QuarkXPress v 2.0.0.0"));
                dataSource.Add(new Items("XLIFF", "XLIFF 1.1-1.2 v 2.0.0.0"));
                dataSource.Add(new Items("MemoQ", "MemoQ v 1.0.0.0"));
                dataSource.Add(new Items("PDF", "PDF v 2.0.0.0"));
                dataSource.Add(new Items("Java (properties)", "Java Resources v 2.0.0.0"));
                dataSource.Add(new Items("Portable Object", "PO files v 1.0.0.0"));
                dataSource.Add(new Items("SubRip", "SubRip v 1.0.0.0"));
                dataSource.Add(new Items(".NET (resx)", "XML: RESX v 1.2.0.0"));
                dataSource.Add(new Items("ITS", "XML: ITS 1.0 v 1.2.0.0"));
                dataSource.Add(new Items("Plain Text (txt)", "Plain Text v 1.0.0.0"));
                dataSource.Add(new Items("TTX", "TTX 2.0 v 2.0.0.0"));
                dataSource.Add(new Items("ITD", "ITD v 1.0.0.0"));
                //dataSource.Add(new Items("--- CLEAR ---", ""));

            }

            textBox1.KeyPress += textBox1_KeyPress;

            comboBoxFT.DataSource = dataSource;
            comboBoxFT.DisplayMember = "Name";
            comboBoxFT.ValueMember = "Value";
            comboBoxFT.SelectedIndexChanged += buttonSet_SelectedIndexChanged;
            comboBoxFT.KeyPress += comboBoxFT_OnKeyPress;

            textBoxSRC.TextChanged += textBoxSRC_TextChanged;
            textBoxTGT.TextChanged += textBoxTGT_TextChanged;

            buttonSet.Enabled = false;
            this.comboBoxFT.TextChanged += buttonSet_TextChanged;

            if (!Settings.LANG_CHANGE)
            {
                ToggleLanguageFields(false);
            }
            else
            {
                ToggleLanguageFields(true);
            }
            
        }

        void textBoxTGT_TextChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
                buttonSet.Enabled = true;
        }
        void textBoxSRC_TextChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
                buttonSet.Enabled = true;
        }

        void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                LoadFiles();
            }
        }

        void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A && e.Control)
            {
                listView1.MultiSelect = true;
                foreach (ListViewItem item in listView1.Items)
                {
                    item.Selected = true;
                }
            }
        }

        private void rcUntask_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Selected)
                {
                    UntaskItem(item);
                }
            }
        }

        void ContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;

            if (listView1.SelectedItems.Count != 1)
            {
                menu.Items[6].Enabled = false;
                menu.Items[7].Enabled = false;
            }
            else
            {
                menu.Items[6].Enabled = true;
                menu.Items[7].Enabled = true;
            }
        }

        private void rcCopyFolderPath_Click(object sender, EventArgs e)
        {
            string originalFilePath = listView1.SelectedItems[0].ToolTipText.Replace(toolTipPrefix, "");

            Clipboard.SetText(originalFilePath);
        }

        private void rcOpenFolder_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                string originalFilePath = Path.GetDirectoryName(listView1.SelectedItems[0].ToolTipText.Replace(toolTipPrefix, ""));

                try
                {
                    Process.Start(@originalFilePath);
                }
                catch (Exception openFolderExc)
                {
                    MessageBox.Show("Couldn't access the original folder (it may be placed on another machine or unavailable network drive): \n\n" + originalFilePath, "SDLXLIFF Companion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void rcSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                item.Selected = true;
            }
        }

        private void rcRemoveAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                item.Remove();
            }

            FileList.CleanSourceList();
            FileList.CleanTargetList();
        }

        private void rcRemove_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Selected)
                {
                    item.Remove();
                    FileList.RemoveSourceItem(GetItemPath(item));
                    FileList.RemoveTargetItem(GetItemPath(item));
                }
            }
        }

        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //
        }

        private string GetDefaultName()
        {
            string name = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            name += "\\FTFIX_filetypes.xml";

            return name;
        }


        void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != sortColumn)
            {
                // Set the sort column to the new column.
                sortColumn = e.Column;
                // Set the sort order to ascending by default.
                listView1.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (listView1.Sorting == SortOrder.Ascending)
                    listView1.Sorting = SortOrder.Descending;
                else
                    listView1.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            listView1.Sort();
            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            this.listView1.ListViewItemSorter = new ListViewItemComparer(e.Column,
                                                              listView1.Sorting);
        }

        private string userPath = "";
        private Color COL_OK = Color.Black;
        private Color COL_NOT_REC = Color.Red;
        private Color COL_GUESSED = Color.LightSkyBlue;
        private Color COL_TO_UPDATE = Color.LimeGreen;

        private string STATE_TO_UPDATE = "to update";
        private string STATE_GUESSED = "recognized";
        private string STATE_GUESSED_FROM_OTHER = "recognized (from source file)";

        private void buttonSet_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ComboBox combobox = (ComboBox)sender;
            string ID = (string)combobox.SelectedValue;
            if (listView1.SelectedItems.Count > 0)
                this.buttonSet.Enabled = true;
        }
        private void buttonSet_TextChanged(object sender, System.EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
                this.buttonSet.Enabled = true;
        }

        protected void comboBoxFT_OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ListView.SelectedListViewItemCollection items = this.listView1.SelectedItems;
                string newID;
                string comboBoxFTValue = "";
                string comboBoxSelectedText = "";
                comboBoxFTValue += (string)comboBoxFT.SelectedValue;

                comboBoxSelectedText = (string)comboBoxFT.SelectedText;

                if (comboBoxFTValue.Length < 1 && comboBoxSelectedText != "--- CLEAR ---")
                {
                    newID = (string)comboBoxFT.Text;
                    this.AddToComboBoxFT(newID, newID);
                }
                else
                {
                    newID = (string)comboBoxFT.SelectedValue;
                }

                foreach (ListViewItem item in items)
                {
                    //item.SubItems[1].Text = newID;
                    SetItemID(item, newID);
                    //item.SubItems[2].Text = "to update";
                    SetItemStatus(item, "to update");
                    //item.SubItems[3].Text = textBoxSRC.Text;
                    //item.SubItems[4].Text = textBoxTGT.Text;
                    item.ForeColor = COL_TO_UPDATE;
                }

                buttonSet.Enabled = false;
            }
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection items = this.listView1.SelectedItems;
            string newID;
            string comboBoxFTValue = "";
            string comboBoxSelectedText = "";
            comboBoxFTValue += (string)comboBoxFT.SelectedValue;
            
            comboBoxSelectedText = (string)comboBoxFT.SelectedText;

            if (comboBoxFTValue.Length < 1 && comboBoxSelectedText != "--- CLEAR ---")
            {
                newID = (string)comboBoxFT.Text;
                this.AddToComboBoxFT(newID, newID);
            }
            else
            {
                newID = (string)comboBoxFT.SelectedValue;
            }
            
            foreach (ListViewItem item in items)
            {
                // set new file type ID
                if (comboBoxFT.Text != "")
                {
                    SetItemID(item, newID);
                    SetItemStatus(item, "to update");
                }

                // set new language
                if (Settings.LANG_CHANGE)
                {
                    SetItemSRC(item, textBoxSRC.Text);
                    SetItemTGT(item, textBoxTGT.Text);
                }

                item.ForeColor = COL_TO_UPDATE;
            }

            buttonSet.Enabled = false;

        }

        private void RefreshComboBox()
        {
            Console.WriteLine("Refreshing");
            comboBoxFT.DataSource = null;
            comboBoxFT.DataSource = dataSource;
            comboBoxFT.DisplayMember = "Name";
            comboBoxFT.ValueMember = "Value";
            comboBoxFT.Refresh();
        }

        public void SetComboBox(Dictionary<string, string> newContent)
        {
            Console.WriteLine("Setting new combobox");
            dataSource.Clear();

            foreach (var item in newContent)
            {
                dataSource.Add(new Items(item.Key, item.Value));
            }
            this.RefreshComboBox();
        }

        private void AddToComboBoxFT(string name, string value)
        {
            if (name != "" && value != "") 
            {
                Items newItem = new Items(name, value);
                bool found = false;

                foreach (Items item in dataSource)
                {
                    if (item.Value == name)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    dataSource.Add(new Items(name, value));
                    comboBoxFT.DataSource = null;
                    comboBoxFT.DataSource = dataSource;
                    comboBoxFT.DisplayMember = "Name";
                    comboBoxFT.ValueMember = "Value";
                    comboBoxFT.Refresh();
                    fileTypes.AddTypeXML(name, value);
                    fileTypes.SaveAsXML();
                }
            }
        }
        
        private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ListView.SelectedListViewItemCollection items = this.listView1.SelectedItems;

            string ftID = "null";
            string srcLang = "null";
            string tgtLang = "null";

            foreach (ListViewItem item in items)
	        {
                if (ftID == "null" || ftID == GetItemID(item))
                {
                    ftID = GetItemID(item);
                }
                else
                {
                    ftID = "* multiple *";
                }

                if (srcLang == "null" || srcLang == GetItemSRC(item))
                {
                    srcLang = GetItemSRC(item);
                }
                else
                {
                    srcLang = "";
                }

                if (tgtLang == "null" || tgtLang == GetItemTGT(item))
                {
                    tgtLang = GetItemTGT(item);
                }
                else
                {
                    tgtLang = "";
                }

	        }

            try
            {
                comboBoxFT.SelectedValue = ftID;
            }
            catch (Exception comboException)
            {
                Console.WriteLine(comboException.Message);
                comboBoxFT.Text = ftID;
            }

            if (srcLang != "")
            {
                //textBoxSRC.Enabled = true;
                textBoxSRC.Text = srcLang;
            }
            else
            {
                textBoxSRC.Text = "";
                //textBoxSRC.Enabled = false;
            }
            if (tgtLang != "")
            {
                //textBoxTGT.Enabled = true;
                textBoxTGT.Text = tgtLang;
            }
            else
            {
                textBoxTGT.Text = "";
                //textBoxTGT.Enabled = false;
            }

            buttonSet.Enabled = false;
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            textBox1.Text = "";
            //listView1.Items.Clear();
            //FileList.CleanSourceList();
            //FileList.CleanTargetList();

            bool userInformed = false;

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            progressBar1.Maximum = files.Length;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            foreach (string file in files)
            {
                progressBar1.Value++;
                if (file.Substring(file.Length - 8).ToLower() == "sdlxliff")
                {
                    if (!this.Exists(file))
                    {
                        userPath = "not empty!";

                        System.Console.WriteLine(file);

                        SdlxliffEval(file);
                    }
                }
                else if (Directory.Exists(file) && !userInformed)
                {
                    MessageBox.Show("In order to fix files from specific folder (and its subfolders) paste the path into the text field.", "SDLXLIFF Companion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    userInformed = true;
                }
            }

            SetFTFromSource();

            progressBar1.Value = 0;


        }

        private bool Exists(string filePath)
        {
            bool exists = false;

            foreach (ListViewItem item in listView1.Items)
            {
                if (GetItemPath(item) == filePath)
                {
                    exists = true;
                }
            }

            return exists;
        }

        // 'Load' button
        private void button1_Click(object sender, EventArgs e)
        {
            LoadFiles();
        }

        // 'Repair' button
        private void button2_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = listView1.Items.Count;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            bool changeMade;
            bool dependencyNotification;

            string currFile = "";
            Sdlxliff sdlxliff = null;

            foreach (ListViewItem item in listView1.Items)
                {
                    progressBar1.Value++;
                    changeMade = false;
                    string newFT = GetItemStatus(item);
                    int r = item.ImageIndex;

                    if (newFT == STATE_GUESSED || newFT == STATE_TO_UPDATE || newFT == STATE_GUESSED_FROM_OTHER || (r >= 0 && Settings.DEPENDENCY_FIX))
                    {
                        currFile = GetItemUPath(item);
                        sdlxliff = new Sdlxliff(currFile);
                        sdlxliff.SaveBackup();
                    }

                    if (item.ForeColor == COL_TO_UPDATE)
                    {
                        sdlxliff.SetSrcLang(GetItemSRC(item));
                        sdlxliff.SetTgtLang(GetItemTGT(item));
                        changeMade = true;
                    }

                    if (r >= 0 && Settings.DEPENDENCY_FIX)
                    {
                        Console.WriteLine("Fixing dependency!");
                        string origFilePath = sdlxliff.GetOriginalFilePath();
                        string extFile = sdlxliff.externalFile;
                        string tgtLang = sdlxliff.GetTgtLang();

                        if (currFile.Contains(tgtLang))
                        {
                            string srcLang = sdlxliff.GetSrcLang();
                            string sourceFile = currFile.Replace("\\" + tgtLang + "\\", "\\" + srcLang + "\\");
                            sourceFile = sourceFile.Replace(Path.GetFileName(sourceFile), Path.GetFileName(origFilePath));

                            if (File.Exists(sourceFile))
                            {
                                sdlxliff.SetExternalFile("file://" + sourceFile);
                                //item.SubItems[5].Text = "";
                                SetItemExtStatus(item, -1);
                                Console.WriteLine("Dependency fixed!");
                                changeMade = true;
                            }
                            else if (extFile.Contains("\\AppData\\Local\\Temp\\"))
                            {
                                DialogResult origFileNotFoundDialog = DialogResult.Yes;

                                if (!Settings.DEPENDENCY_FIX_ORIG_FILE)
                                {
                                    origFileNotFoundDialog = MessageBox.Show("Original dependency file can't be accessed:\n" + sdlxliff.GetOriginalFilePath() + "\n\nUse this path for dependency file anyway?", "SDLXLIFF Companion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                                }
                                if (origFileNotFoundDialog == DialogResult.Yes)
                                {
                                    sdlxliff.SetExternalFile("file://" + sdlxliff.GetOriginalFilePath());
                                    //item.SubItems[5].Text = "";
                                    SetItemExtStatus(item, -1);
                                    Console.WriteLine("Dependency fixed!");
                                    changeMade = true;
                                }
                            }
                        }
                        else if (extFile.Contains("\\AppData\\Local\\Temp\\"))
                        {
                            DialogResult origFileNotFoundDialog = DialogResult.Yes;

                            if (!Settings.DEPENDENCY_FIX_ORIG_FILE)
                            {
                                origFileNotFoundDialog = MessageBox.Show("Original dependency file can't be accessed:\n" + sdlxliff.GetOriginalFilePath() + "\n\nUse this path for dependency file anyway?", "SDLXLIFF Companion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            }
                            if (origFileNotFoundDialog == DialogResult.Yes)
                            {
                                sdlxliff.SetExternalFile("file://" + sdlxliff.GetOriginalFilePath());
                                //item.SubItems[5].Text = "";
                                SetItemExtStatus(item, -1);
                                Console.WriteLine("Dependency fixed!");
                                changeMade = true;
                            }
                        }
                    }

                    if (newFT == STATE_GUESSED || newFT == STATE_TO_UPDATE || newFT == STATE_GUESSED_FROM_OTHER)// || (r >= 0 && Settings.DEPENDENCY_FIX))
                    {
                        string goodFt = GetItemID(item);
                        string newSRC = GetItemSRC(item);
                        string newTGT = GetItemTGT(item);
                        
                        sdlxliff.SetFileTypeID(goodFt);

                        SetItemStatus(item, "OK");
                        item.ForeColor = COL_OK;
                        changeMade = true;
                    }

                    if (changeMade)
                    {
                        SetItemExtStatus(item, -1);
                        sdlxliff.Save();
                    }

                    if (r == 3)
                    {
                        Console.WriteLine("currFile: " + currFile);

                        SdlxliffDetails sdlxliffDetails = FileList.GetTargetFile(currFile);

                        if (sdlxliffDetails == null)
                        {
                            sdlxliffDetails = FileList.GetSourceFile(currFile);
                        }

                        try
                        {
                            sdlxliff.Rename(sdlxliffDetails.untaskedFilePath);
                            sdlxliff.SetFilePath(sdlxliffDetails.untaskedFilePath);
                        }
                        catch (System.IO.IOException ex)
                        {
                            DialogResult dialogResult = MessageBox.Show("File with the same name already exists:\n" + sdlxliffDetails.untaskedFilePath + "\n\nFile will be skipped.", "SDLXLIFF Companion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            SetItemPath(item, sdlxliffDetails.filePath);
                        }

                        SetItemExtStatus(item, -1);

                    }
                }
            listView1.Refresh();
            progressBar1.Value = 0;
            buttonSet.Enabled = false;
            
        }

        private XmlDocument LookForSourceFile(string filePath, string tgtLang, string srcLang)
        {
            XmlDocument src = new XmlDocument();

            try
            {
                filePath = filePath.Replace(tgtLang, srcLang);
                src.Load(filePath);
                Console.WriteLine("Loaded: " + filePath);
            }
            catch (Exception srcXmlLoad)
            {
                Console.WriteLine(srcXmlLoad.ToString());
                return null;
            }

            return src;
        }
        private void SetFTFromSource()
        {
            progressBar1.Maximum = FileList.SOURCE_FILES.Count();
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            foreach (SdlxliffDetails src in FileList.SOURCE_FILES)
            {
                progressBar1.Value++;
                
                foreach (ListViewItem file in listView1.Items)
                {
                    string filePath, id, srcLang, tgtLang, state;

                    //filePath = file.SubItems[0].Text;
                    filePath = GetItemPath(file);
                    //id = file.SubItems[1].Text;
                    id = GetItemID(file);
                    //state = file.SubItems[2].Text;
                    state = GetItemStatus(file);
                    //srcLang = file.SubItems[3].Text;
                    srcLang = GetItemSRC(file);
                    //tgtLang = file.SubItems[4].Text;
                    tgtLang = GetItemTGT(file);

                    if (tgtLang != "" && src.fileTypeID != "" && (state == STATE_GUESSED || id.Trim() == ""))
                    {
                        // COMPARE FILE PATH
                        if (filePath.Replace("\\" + tgtLang + "\\", "\\" + srcLang + "\\") == src.filePath)
                        {
                            SetItemGuessedFromOther(file, src.fileTypeID);
                        }
                        // COMPARE INTERNAL FILE PATH
                        else
                        {
                            //Sdlxliff source = new Sdlxliff(src.Key);
                            //Sdlxliff target = new Sdlxliff(filePath);

                            SdlxliffDetails target = FileList.GetTargetFile(filePath);

                            if (target != null && src.fileTypeID != "" && src.originalFilePath == target.originalFilePath)
                            {
                                SetItemGuessedFromOther(file, src.fileTypeID);
                            }
                        }
                    }
                }
            }

            progressBar1.Value = 0;
        }

        private List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();
            files.Add(sDir);
            
            try
            {
                foreach (string f in Directory.GetDirectories(sDir))
                {
                    files.Add(f);
                    Console.WriteLine("Adding folder: " + f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    List<String> temp = DirSearch(d);

                    foreach (string s in temp)
                    {
                        if (!files.Contains(s))
                            files.Add(s);
                    }
                    
                    //files.AddRange(DirSearch(d));
                }
            }
            catch (System.Exception excpt)
            {
                MessageBox.Show(excpt.Message);
            }

            return files;
        }

        // 'Settings' button
        private void button3_Click(object sender, EventArgs e)
        {
            bool isVisible = false;

            foreach (Form form in Application.OpenForms)
            {
                if (form.Name == "FTSettings")
                {
                    isVisible = true;
                    form.Activate();
                }
            }

            if (!isVisible)
            {
                FTSettings settings;

                settings = new FTSettings(xmlPath);

                Console.WriteLine(xmlPath);
                settings.SetMainForm(this);
                settings.StartPosition = FormStartPosition.CenterParent;
                settings.ShowDialog();
            }
        }

        private void SdlxliffEval(String file)
        {
            string guessedFT;
            string comment;
            bool extFileMissing = false;

            Color statusColor = Color.Black;

            Sdlxliff newSdlxliff = new Sdlxliff(file);

            string fileStatus = "";

            string sFileTypeID = newSdlxliff.GetFileTypeID();

            if (newSdlxliff.externalFile.Contains("\\AppData\\Local\\Temp\\"))
            {
                extFileMissing = true;
            }

            if (sFileTypeID == "")
            {
                //guessedExt = newSdlxliff.GuessFT();
                guessedFT = newSdlxliff.GuessFT();

                if (guessedFT == "")
                {
                    comment = "";
                    fileStatus = "not recognized";
                    statusColor = COL_NOT_REC;
                }
                else
                {
                    comment = guessedFT;
                    fileStatus = STATE_GUESSED;
                    statusColor = COL_GUESSED;
                }

            }
            else
            {
                comment = sFileTypeID;
                fileStatus = "OK";
                statusColor = COL_OK;
            }

            string sourceLang;
            string targetLang;

            try
            {
                sourceLang = newSdlxliff.GetSrcLang();
            }
            catch (Exception sourceExcept)
            {
                sourceLang = "";
            }

            try
            {
                targetLang = newSdlxliff.GetTgtLang();
            }
            catch (Exception targetExcept)
            {
                targetLang = "";
            }

            string originalFilePath = newSdlxliff.GetOriginalFilePath();

            AddToComboBoxFT(comment, comment);

            ListViewItem item = new ListViewItem(new[] { "", file, comment, fileStatus, sourceLang, targetLang, file });
            if (!newSdlxliff.isSource)
                FileList.TARGET_FILES.Add(new SdlxliffDetails(file, originalFilePath, sFileTypeID, sourceLang, targetLang, ""));
            else
                FileList.SOURCE_FILES.Add(new SdlxliffDetails(file, originalFilePath, sFileTypeID, sourceLang, targetLang, ""));
            item.ForeColor = statusColor;
            item.ToolTipText = "Original file path: \n" + newSdlxliff.GetOriginalFilePath();

            if (extFileMissing)
            {
                if (GetItemExtStatus(item) == 3)
                {
                    SetItemExtStatus(item, 4);
                }
                else
                {
                    SetItemExtStatus(item, 2);
                }

                //item.ImageIndex = 2;
                //item.ToolTipText = "Missing Dependency File";
            }
            //item.SubItems[0].Font = new Font(item.SubItems[0].Font, FontStyle.Italic);
            listView1.Items.Add(item);

            if (Settings.UNTASK_AUTO)
            {
                UntaskItem(item);
            }

            newSdlxliff = null;
        }

        private void SetItemGuessedFromOther(ListViewItem item, string fileType)
        {
            //item.SubItems[2].Text = STATE_GUESSED_FROM_OTHER;
            SetItemStatus(item, STATE_GUESSED_FROM_OTHER);
            //item.SubItems[1].Text = fileType;
            SetItemID(item, fileType);
            item.ForeColor = COL_GUESSED;

        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            bool isVisible = false;

            foreach (Form form in Application.OpenForms)
            {
                if (form.Name == "FTSettings")
                {
                    isVisible = true;
                    form.Activate();
                }
            }

            if (!isVisible)
            {
                FTSettings settings;

                settings = new FTSettings(xmlPath);

                Console.WriteLine(xmlPath);
                settings.SetMainForm(this);
                settings.StartPosition = FormStartPosition.CenterParent;
                settings.ShowDialog();
            }
        }

        private int GetItemExtStatus(ListViewItem item)
        {
            return item.ImageIndex;
        }
        private string GetItemPath(ListViewItem item)
        {
            return item.SubItems[pathIndex].Text;
        }
        private string GetItemID(ListViewItem item)
        {
            return item.SubItems[idIndex].Text;
        }
        private string GetItemStatus(ListViewItem item)
        {
            return item.SubItems[statusIndex].Text;
        }
        private string GetItemSRC(ListViewItem item)
        {
            return item.SubItems[srcIndex].Text;
        }
        private string GetItemTGT(ListViewItem item)
        {
            return item.SubItems[tgtIndex].Text;
        }
        private string GetItemUPath(ListViewItem item)
        {
            return item.SubItems[uPathIndex].Text;
        }
        private string GetItemLangChange(ListViewItem item)
        {
            return item.SubItems[itemLangChangeIndex].Text;
        }

        private void SetItemExtStatus(ListViewItem item, int imageIndex) 
        {
            item.ImageIndex = imageIndex;
        }
        private void SetItemPath(ListViewItem item, string path)
        {
            item.SubItems[pathIndex].Text = path;
        }
        private void SetItemID(ListViewItem item, string id)
        {
            item.SubItems[idIndex].Text = id;
        }
        private void SetItemStatus(ListViewItem item, string status)
        {
            item.SubItems[statusIndex].Text = status;
        }
        private void SetItemSRC(ListViewItem item, string src)
        {
            item.SubItems[srcIndex].Text = src;
        }
        private void SetItemTGT(ListViewItem item, string tgt)
        {
            item.SubItems[tgtIndex].Text = tgt;
        }
        private void SetItemUPath(ListViewItem item, string uPath)
        {
            item.SubItems[uPathIndex].Text = uPath;
        }
        private void SetItemLangChange(ListViewItem item, string lc)
        {
            item.SubItems[itemLangChangeIndex].Text = lc;
        }

        private void UntaskItem(ListViewItem item)
        {
            string originalFilePath = GetItemPath(item);
            string fileName = Path.GetFileName(originalFilePath);
            string filePath = Path.GetDirectoryName(originalFilePath) + "\\";
            string sourceFileExtension = Path.GetExtension(item.ToolTipText.Replace(toolTipPrefix, ""));

            sourceFileExtension = sourceFileExtension.Replace(".", "_");

            string regExPattern = "(TASK[0-9]+_[A-Z-]+_)(CVTITEM_|CVTTASK_)?(.*?)(" + sourceFileExtension + "|_sdlxliff)?(.sdlxliff)";

            Regex reg = new Regex(regExPattern);

            if (reg.Match(fileName).Success)
            {
                string untaskedPath = "";

                if (!Settings.UNTASK_SUFFIX)
                {
                    untaskedPath = filePath + reg.Matches(fileName)[0].Groups[3].ToString() + reg.Matches(fileName)[0].Groups[4].ToString() + reg.Matches(fileName)[0].Groups[5].ToString();
                }
                else
                {
                    untaskedPath = filePath + reg.Matches(fileName)[0].Groups[3].ToString() + reg.Matches(fileName)[0].Groups[5].ToString();
                }

                SetItemPath(item, untaskedPath);

                if (GetItemExtStatus(item) == 2)
                {
                    SetItemExtStatus(item, 4);
                }
                else
                {
                    SetItemExtStatus(item, 3);
                }

                SdlxliffDetails fileForUntask;
                fileForUntask = FileList.GetTargetFile(originalFilePath);//.SetUntaskedFilePath(untaskedPath);
                if (fileForUntask == null)
                {
                    fileForUntask = FileList.GetSourceFile(originalFilePath);
                }
                fileForUntask.SetUntaskedFilePath(untaskedPath);
            }

            string regExPatternSuffix = "(TASK[0-9]+_[A-Z-]+_)?(CVTITEM_|CVTTASK_)?(.*?)(" + sourceFileExtension + "|_sdlxliff)(.sdlxliff)";

            Regex regSuffix = new Regex(regExPatternSuffix);

            if (Settings.UNTASK_SUFFIX && regSuffix.Match(fileName).Success)
            {
                string untaskedPath = "";

                if (!Settings.UNTASK_SUFFIX)
                {
                    untaskedPath = filePath + regSuffix.Matches(fileName)[0].Groups[3].ToString() + regSuffix.Matches(fileName)[0].Groups[4].ToString() + regSuffix.Matches(fileName)[0].Groups[5].ToString();
                }
                else
                {
                    untaskedPath = filePath + regSuffix.Matches(fileName)[0].Groups[3].ToString() + regSuffix.Matches(fileName)[0].Groups[5].ToString();
                }

                SetItemPath(item, untaskedPath);

                if (GetItemExtStatus(item) == 2)
                {
                    SetItemExtStatus(item, 4);
                }
                else
                {
                    SetItemExtStatus(item, 3);
                }

                SdlxliffDetails fileForUntask;
                fileForUntask = FileList.GetTargetFile(originalFilePath);//.SetUntaskedFilePath(untaskedPath);
                if (fileForUntask == null)
                {
                    fileForUntask = FileList.GetSourceFile(originalFilePath);
                }
                fileForUntask.SetUntaskedFilePath(untaskedPath);
            }
        }
        private void LoadFiles()
        {
            userPath = textBox1.Text;
            FileList.CleanSourceList();
            FileList.CleanTargetList();

            if (userPath != "" && Directory.Exists(userPath))
            {
                List<string> subfolders;
                int subfolderCount = 0;

                // check if user path ends with \ and add it if not
                if (userPath.Substring(userPath.Length - 1) != "\\")
                {
                    userPath += "\\";
                    textBox1.Text = userPath;
                }

                foreach (string f in Directory.GetDirectories(userPath))
                {
                    subfolderCount++;
                }

                if (subfolderCount > 0)
                {
                    // ask if subfolders should be searched
                    DialogResult dialogResult = MessageBox.Show(subfolderCount + " subfolder(s) found, search them for .sdlxliff files?", "SDLXLIFF Companion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        subfolders = DirSearch(userPath);
                    }
                    else
                    {
                        subfolders = new List<string>();
                        subfolders.Add(userPath);
                    }
                }
                else
                {
                    subfolders = new List<string>();
                    subfolders.Add(userPath);
                }

                listView1.Items.Clear();

                string filePath = "";
                int fileCount = 0;
                string slash = "";

                foreach (string folder in subfolders)
                {
                    Console.WriteLine(folder);
                    DirectoryInfo currFolder = new DirectoryInfo(folder);

                    foreach (FileInfo currFile in currFolder.GetFiles("*.sdlxliff"))
                        fileCount++;

                    progressBar1.Maximum = fileCount;
                    progressBar1.Step = 1;
                    progressBar1.Value = 0;

                    foreach (FileInfo currFile in currFolder.GetFiles("*.sdlxliff"))
                    {
                        progressBar1.Value++;
                        Console.WriteLine(currFile.Name);

                        Color statusColor = Color.Black;

                        if (folder.Substring(folder.Length - 1) == "\\")
                        {
                            slash = "";
                        }
                        else
                        {
                            slash = "\\";
                        }

                        filePath = folder + slash + currFile.Name;

                        SdlxliffEval(filePath);
                    }
                }
            }
            else
            {
                //label1.Text = "No such directory!";
            }

            SetFTFromSource();

            progressBar1.Value = 0;
        }

        public void ToggleLanguageFields(bool t)
        {
            textBoxSRC.Enabled = t;
            textBoxTGT.Enabled = t;
        }
    }
}
