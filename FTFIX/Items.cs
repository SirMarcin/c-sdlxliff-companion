﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTFIX
{
    class Items
    {
        public Items(string n, string v)
        {
            Name = n;
            Value = v;
        }

        public string Name { get; set; }
        public string Value { get; set; }
    }
}
